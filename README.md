# CATI CODEX site web

© 2023-2025 - GPL3 License - INRAE

Ce site a été créé en utilisant la technologie [Quarto](https://quarto.org).

Creation: 

* [@isabelle.alic](https://forgemia.inra.fr/isabelle.alic) (INRAE MISTEA), 
* [@arnaud.charleroy](https://forgemia.inra.fr/arnaud.charleroy) (INRAE MISTEA), 
* [@virginie.rossard](https://forgemia.inra.fr/virginie.rossard) (INRAE LBE) et 
* [@isabelle.sanchez](https://forgemia.inra.fr/isabelle.sanchez) - [web site](https://mistea.pages.mia.inra.fr/isabellesanchez/) (INRAE MISTEA) pour le CATI CODEX.
          
https://codex.pages.mia.inra.fr/siteweb/
