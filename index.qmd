---
title: "Présentation ![](img/logo_CODEX.png){#logo1 .center width=20% }"
---

<meta name="description" content="CATI CODEX - INRAE.">

Le CATI CODEX (COnnaissance et Données EXpérimentales)  fédère un ensemble de compétences coordonnées et homogènes en calcul scientifique et en informatique afin de proposer une approche intégrée du traitement des données expérimentales, en alliant des savoir-faire relevant de la gestion de données, de la représentation des connaissances et des statistiques.

Le CATI CODEX construit une offre (méthodes et outils logiciels) pour permettre le traitement complet de la donnée, de l'acquisition à l'analyse, en soutien aux recherches mobilisant des données expérimentales dynamiques multi-échelles (phénotypage haut débit plantes-levures, filière vigne).

**Animateur.ice.s** : Isabelle Alic et Eric Latrille

**Responsables scientifiques** : Nadine Hilgert et Pascal Neveu

Le CATI est rattaché au département Mathnum. Il est composé de membres issus des départements: EA, SPE, GAP, CEPIA, AlimH, MICA.

Le CATI CODEX s'organise en 3 axes métiers et un axe transversal qui interagissent ensembles :

**Axe 1 - Robots, instruments et acquisition automatisée de données**: Cet axe s'intéresse aux développements de méthodes et de protocoles pour la réalisation d'actions automatisées à partir de collectes intelligentes de données dans le domaine de l'agriculture et l'environnement. Les dispositifs matériels considérés vont du capteur sans fil contraint au robot évolué. Le passage de l'acquisition à l'action passe par différents types de logiciels allant des systèmes d'exploitation et d'information aux outils d'aide à la décision.  (Animateurs Gil De Sousa et Jean Laneurit) 

**Axe 2 - Systèmes d’Informations de données et connaissances** : Cet axe concerne la production de modèles de données (base de données, XML) ainsi que la représentation de connaissances (graphe sémantique, ontologie, etc.). L’axe comprend également le développement de logiciels avec un savoir faire autour de la mutualisation et de la diffusion  (Animatrices Anne Tireau et Virginie Rossard)

**Axe 3 - Modélisation, valorisation et partage** (statistiques, IA, ...): Cet axe traite d’une part de valorisation des données, c’est à dire traitement de la donnée brute complexe (IA, fusion de données -type de capteurs, séries temporelles, résolutions spatiales) pour obtenir une information (nouveaux traits, meilleure précision, fréquence, couverture) exploitable pour les applications (généticiens, modélisateurs, communauté phénotypage). Il traite également de mise à disposition et de partage d’outils (packages R, projet d’annotation CVAT, Bibliothèque de Deep Learning, etc.). Ce partage se fait au-delà du CATI, avec notamment le montage de projets SAPI et l’organisation de séminaires inter-CATI. (Animatrices Marie Weiss et Isabelle Sanchez)

**Axe transversal - Infrastructures pour le traitement de données haut débit** : Cet axe vise les ressources pour le stockage, la sécurisation, la conservation, la distribution et le transfert d’informations numériques ainsi que les ressources pour le “calcul” (CPU, GPU) et leur accès (protocoles, architectures, etc). L’objectif de l’axe est de travailler sur les méthodes pour la mise en oeuvre de ces ressources (Animateurs Renaud Colin et Stephan Bernard)
    
Les animateur.rice.s d'axes sont chargé.e.s de favoriser les interactions, la valorisation et la bonne diffusion au sein des communautés.

Le fonctionnement du CATI est organisé autour :

* d'un bureau composé des 2 responsables ainsi que des animateur.ice.s des axes;
* d'un comité de pilotage et de suivi.

![](img/logo_RFinrae.png){#logo2 .center width=50% }
