---
title: "A propos"
---

Le code de ce site est consultable: [https://forgemia.inra.fr/codex/siteweb](https://forgemia.inra.fr/codex/siteweb){target="_blank"}.

## Crédits

### Crédits photographiques
Chaque photo appartient au CATI CODEX

### Graphisme
INRAE

### Droit d’auteur - Copyright © - réutilisation des contenus
© 2023-2025 - GPL3 License - INRAE

La reproduction de tout ou partie du contenu de ce site est soumise à l’autorisation préalable d'INRAE, qui doit être demandée à l'éditeur du site que vous trouverez dans les mentions légales.

### Outil de gestion de contenu
Ce site a été créé en utilisant la technologie [Quarto](https://quarto.org){target="_blank"}.

## Mentions Légales

### Identification du site
Nom du site : CATI CODEX, Centre Automatisé de Traitement de l'Information des COnnaissances et Données EXperimentales

URL complète du site : https://codex.pages.mia.inra.fr

### Éditeur(s) du site

INSTITUT NATIONAL DE LA RECHERCHE POUR L'AGRICULTURE, L'ALIMENTATION ET L'ENVIRONNEMENT
est un établissement public à caractère scientifique et technologique. Ses statuts sont publiés dans le code rural et de la pêche maritime (articles R831-1 et suivants).

Président-Directeur général : M. Philippe MAUGUIN

Siège : 147 rue de l'Université, 75338 Paris cedex 07

Contact mail : web@inrae.fr – Contact téléphonique : + 33 (0)1 42 75 00 00

Mathématiques, Informatique et STatistique pour l'Environnement et l'Agronomie (MISTEA)
Est une unité INRAE

Directrice d'unité : Nadine HILGERT

Adresse : 2 Pierre Viala 34000 Montpellier

Contact : webmistea

### Numéros d'identification
INRAE

SIREN : 180 070 039

Code APE : 7219Z

Numéro de TVA intracommunautaire : FR 57 1800700039

### Publication du site
Nom des directeur.ice.s de publication : Philippe MAUGUIN et Nadine HILGERT

Nom des responsables de rédaction : Isabelle ALIC, Eric LATRILLE (responsables du CATI CODEX)

Webmaster du site : 

* [Isabelle Alic](https://forgemia.inra.fr/isabelle.alic){target="_blank"} (INRAE MISTEA), 
* [Arnaud Charleroy](https://forgemia.inra.fr/arnaud.charleroy){target="_blank"} (INRAE MISTEA), 
* [Virginie Rossard](https://forgemia.inra.fr/virginie.rossard){target="_blank"} (INRAE LBE) et 
* [Isabelle Sanchez](https://mistea.pages.mia.inra.fr/isabellesanchez/){target="_blank"} (INRAE MISTEA) 

### Hébergeur du site
Les serveurs de forgemia, qui héberge ce site web, sont des VM hébergées par la DSI INRAE dans le DataCenter INRAE de Toulouse.

## Conditions Générales d'Utilisation

En navigant sur ce site, vous reconnaissez, en votre qualité d'utilisateur, connaître et accepter les termes des conditions d'utilisations décrites ci-dessous.

### Informations générales
Se reporter aux mentions légales du site 

### Propriété intellectuelle
La structure générale du site, les contenus, les textes, y compris les communiqués de presse, les vidéos, les images et les animations qui apparaissent ou sont disponibles sur le site, sont protégés par le droit de la propriété intellectuelle de tiers. 

A ce titre, vous vous engagez à ne pas copier, traduire, reproduire, vendre, publier, exploiter et diffuser des contenus du site, protégés par le droit de la propriété intellectuelle, sans autorisation préalable et écrite de l'éditeur du site, que vous trouverez dans les mentions légales.
L’éditeur du site consent toutefois à l'utilisateur le droit de reproduire tout ou partie du contenu du site, en un exemplaire pour copie de sauvegarde ou tirage sur papier. Ce droit est consenti dans le cadre d'un usage strictement personnel, privé et non collectif.

L’utilisateur a le droit de citer le contenu du site sous réserve du respect de la loi (cf. article L122-5 du code de la Propriété Intellectuelle).

Les marques et les logos présents sur le site sont, sauf cas particulier, la propriété de l’éditeur du site. Sauf autorisation préalable et écrite, vous vous interdisez de les réutiliser, sauf à faire référence au présent site.

La responsabilité de l’éditeur du site ne peut, en aucune manière, être engagée quant aux conséquences pouvant résulter de l’utilisation ou de l’interprétation des informations présentes sur le site. En outre, ces informations sont présentes à titre purement informatif et ne peuvent en aucun cas constituer un conseil ou une recommandation de quelque nature que ce soit. L’utilisateur est seul responsable de l’utilisation qu’il fait des informations présentes sur le site.

L’éditeur du site s'efforce d'assurer au mieux de ses possibilités, l'exactitude et la mise à jour des informations diffusées, au moment de leur mise en ligne sur le site. Cependant, l’éditeur du site ne garantit en aucune façon l'exactitude, la précision ou l'exhaustivité des informations mises à disposition sur le site. Les informations présentes sur le site sont non contractuelles et peuvent être modifiées à tout moment.

L'accès au site peut être interrompu à tout moment, sans préavis, notamment en cas de force majeure, de maintenance ou si l'éditeur du site décide de suspendre ou d'arrêter la fourniture de ce service.

### Liens hypertextes

Le site peut contenir des liens vers des sites de partenaires ou de tiers. L'éditeur du site, ne disposant d'aucun moyen pour contrôler ces sites, n'assume aucune responsabilité quant au contenu de ces sites.
La mise en place de liens hypertextes (qu’elle qu’en soit la modalité technique) par des tiers vers des pages ou des documents diffusés sur le site de l’éditeur du site, est autorisée sous réserve que

* les liens ne contreviennent pas aux intérêts de l’éditeur du site,
* qu'ils garantissent la possibilité pour l'utilisateur d'identifier l'origine et l'auteur du document.

### Données personnelles - Droit d'accès et de rectification
Aucune donnée personnelle n'est recueillie sur le présent site. 

L’INRAE a nommé une Déléguée à la Protection des Données. Vous pouvez la contacter par email à cil-dpo@inra.fr ou par courrier à INRA - 24, chemin de Borde Rouge –Auzeville – CS52627 – 31326 Castanet Tolosan cedex – France.

### Modifications

L’éditeur du site se réserve le droit de modifier, sans préavis, les présentes conditions générales d'utilisation du site.

### Litige

En cas de litige, les tribunaux français seront seuls compétents.

## Cookies
Ce site n'utilise aucun cookie.

